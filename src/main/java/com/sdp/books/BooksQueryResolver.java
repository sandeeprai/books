package com.sdp.books;

import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class BooksQueryResolver implements GraphQLQueryResolver {

	public Book getBookById(String id) {
        return Book.builder().id(id).name("SameName").build();
    }
	

}
