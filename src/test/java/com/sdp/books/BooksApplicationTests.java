package com.sdp.books;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


/**
 * This fails without specifying a port<br>
 * See issue and fix at:
 * https://github.com/graphql-java-kickstart/graphql-spring-boot/issues/146#issuecomment-430363769
 *
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class BooksApplicationTests {

	@Test
	void contextLoads() {
	}

}
