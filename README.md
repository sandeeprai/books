# Books

GraphQL Project for Books Info

*Note:* The project does not expose a GraphQL schema at present, but it will be 
added soon.

## Docker Hub Setup

[Docker Hub](https://hub.docker.com) is used as the container registry to push 
images for this application. AWS Elastic Beanstalk will then pull the image and 
deploy it. 

Setup or use an existing Docker Hub account.  

Define the following Gitlab CI Variables:
* DOCKER_USER - Username of the Docker Hub account
* DOCKER_PWD - Password of the Docker Hub account
* DOCKER_IMG - Name of the image that will be pushed (e.g. `sandeepksd/books`). 
This should match the image name from 
[Dockerrun.aws.json](https://gitlab.com/sandeeprai/books/blob/master/Dockerrun.aws.json)

## AWS Account Setup

The CICD pipeline for this repository deploys the application to AWS Elastic 
Beanstalk.  

Setup or use an existing AWS account with:
* Elastic Beanstalk Environment and Application
* IAM User with AWSElasticBeanstalkFullAccess permission

Define the following Gitlab CI Variables:
* AWS_DEFAULT_REGION - The region of the AWS Account (e.g. `us-east-2`)
* EB_APP - Elastic Beanstalk application name (e.g. `Books`)
* EB_ENV - Elastic Beanstalk environment name (e.g. `Books-env`)
* AWS_ACCESS_KEY_ID - Access Key ID of the IAM User
* AWS_SECRET_ACCESS_KEY - Secret Access Key of the IAM User

## Files for CICD

### Dockerfile
Used by the `Prepare` step to build the image for this application.

### Dockerrun.aws.json
This file describes how to deploy a remote Docker image as an Elastic Beanstalk 
application. Used by `eb cli` in the `Deploy` step.

### .ebignore
This file tells `eb cli` to ignore everything in this repository except 
Dockerrun.aws.json. This ensures that the image pushed to Docker Hub in the 
`Prepare` step is used. Without this file, the image will be built again using 
the Dockerfile. Used by `eb cli` in the `Deploy` step.

### .gitlab-ci.yml
This contains the CICD config explained in detail below.


## CICD Config

See 
[GitLab CI configuration](https://gitlab.com/sandeeprai/books/blob/master/.gitlab-ci.yml)  

These are the Stages:

1. **Build: Build and test the code.**
    * Use **Java 8** base image `openjdk:8-alpine`.  
      In this project, **alpine** version of images are used wherever possible
      because they are lightweight and efficient.
    * Use **Maven wrapper** (mvnw) for Maven build and executing Unit Tests.  
      Run with `-q` option for quiet output (only show errors)  
      See [this](https://github.com/takari/maven-wrapper) for documentation on 
      Maven wrapper.  
      `clean verify` ensures clean build and verification of integration tests
      if any (not applicable in this project as of now)
    * Output of the build is `api.jar`. This is configured in 
      [pom.xml](https://gitlab.com/sandeeprai/books/blob/master/pom.xml) 
      with `finalName`. 
      This name will be used a standard across all projects.   
      Save this artifact for use in future steps in the pipeline.   
      <br>

2. **Prepare: Build and Push the Docker image to Docker Hub**
    * Run Docker in Docker (dind). 
      Use Docker Docker image to execute Docker commands.  
      We can't run Docker commands directly on the Gitlab runner because it 
      doesn't have Docker daemon running.  
    * GitLab CICD Variables are used for Docker Hub credentials and image name.
        * DOCKER_USER
        * DOCKER_PWD
        * DOCKER_IMG (e.g. `sandeepksd/books`)
    * The tag `awsdeploy` will be used as a standard across all projects for 
      the image to be deployed to AWS.   
      <br>

3. **Deploy: Deploy to AWS**    
    * Use image `sandeepksd/ebcli`. This has `ebcli` installed.
      `ebcli` is a Command Line Interface to perform actions like creating apps,
      environments and deploying to AWS Elatic Bean Stalk.  
    * `ebcli` uses the configuration from 
      [Dockerrun.aws.json](https://gitlab.com/sandeeprai/books/blob/master/Dockerrun.aws.json)
      from the root of the repository. It is configured to deploy the image 
      built in the above step with the tag `awsdeploy`.  
    * GitLab CI Variables are used for the AWS region, app name and environment.
        * AWS_DEFAULT_REGION (e.g. `us-east-2`)
        * EB_APP (e.g. `Books`)
        * EB_ENV (e.g. `Books-env`)
    * AWS Credentials are defined as GitLab CI Variables and are available to 
      `ebcli` through environment variables.
        * AWS_ACCESS_KEY_ID
        * AWS_SECRET_ACCESS_KEY
